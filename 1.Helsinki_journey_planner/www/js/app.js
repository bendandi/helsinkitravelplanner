

var app = angular.module('app', ['ionic']);

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});


// PROVIDERS
//local storage
app.factory('localStorage', [function(){

  return {
    getRoutes: function() {
      var routeString = window.localStorage['routes'];
      if(routeString) {
        return angular.fromJson(routeString);
      }
      return [];
    },
    getOrigin: function() {
      var origin = window.localStorage['origin'];
      if(origin) {
        return origin;
      }
      return "";
    },
    getDestination: function() {
      var destination = window.localStorage['destination'];
      if(destination) {
        return destination;
      }
      return "";
    },
    saveRoutes: function(routes, origin, destination) {
      window.localStorage['routes'] = angular.toJson(routes);
      window.localStorage['origin'] = origin;
      window.localStorage['destination'] = destination;
    }
  };
}]);

//Reittiopas API access
app.factory('dataFactory', ['$http', function($http){

  var urlBase = 'http://api.reittiopas.fi/hsl/prod/?user=getHomeHelsinki&pass=getHome';
  dataFactory = {};

  dataFactory.geocode = function(address){
    //prepare street name and house number for GET request parameters
    var x = address.split(" ");
    var street = "";
    var house;
    var url = urlBase;
    for(var i = 0; i < x.length; i++){
      if(isNaN(x[i])){
        street += x[i];
      }
      else{
        house = x[i];
        url += '&request=geocode&key=' + street + "+" + house;
        break;
      };
    };
    url += '&request=geocode&key=' + street;

    return $http.get(url);
  };

  dataFactory.getRoutes = function(origin, destination, transportTypes){

    var url =  urlBase + '&request=route&from=' + origin
    + '&to=' + destination + '&show=5&transport_types=walk';

    if(transportTypes.bus){
      url += '|bus|uline';
    }
    if(transportTypes.train){
      url += '|train|service';
    }
    if(transportTypes.tram){
      url += '|tram';
    }
    if(transportTypes.subway){
      url += '|metro';
    }
    if(transportTypes.ferry){
      url += '|ferry';
    }

    return $http.get(url);
  };

  return dataFactory;

}]);


// CONTROLLERS

app.controller('JourneyPlannerCtrl', ['$scope', '$q','dataFactory', 'localStorage', '$ionicModal',
function($scope,$q, dataFactory, localStorage, $ionicModal) {
  $scope.title = "Helsinki Journey Planner";
  $scope.routes = localStorage.getRoutes();
  $scope.origin = localStorage.getOrigin();
  $scope.destination = localStorage.getDestination();
  $scope.legListToggle = false;
  $scope.shownRoute = null;
  $scope.preloader = "";
  $scope.transportTypes = {
    bus:true,
    train:true,
    subway:true,
    tram:true,
    ferry:true
  };

  // Create and load the route search Modal
  $ionicModal.fromTemplateUrl('input-modal.html', function(modal) {
    $scope.inputModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });

  // Called when the lookup form is submitted
  $scope.getRoutes = function(origin, destination) {
    $scope.modalErrorMessage = "";
    console.log(dataFactory.geocode(origin));

    $q.all([dataFactory.geocode(origin), dataFactory.geocode(destination)])
    .then(
      function(geocode){
        $scope.origin = geocode[0].data[0].matchedName;
        $scope.destination = geocode[1].data[0].matchedName;

        return dataFactory.getRoutes(geocode[0].data[0].coords,
                                      geocode[1].data[0].coords,
                                      $scope.transportTypes);})
    .then(
      function(routes){
        $scope.closeInputModal();
        $scope.routes = routes.data;
        localStorage.saveRoutes(routes.data, $scope.origin, $scope.destination);
      },
      function(){
        $scope.modalErrorMessage = "Drunken? Let's check those inputs";
      });
  };


  //Route details accordion
  $scope.toggleRoute = function(route) {
    if ($scope.isRouteShown(route)) {
      $scope.shownRoute = null;
    } else {
      $scope.shownRoute = route;
    }
    console.log($scope.shownRoute);
  };

  $scope.isRouteShown = function(route) {
    return $scope.shownRoute === route;
  };

  // Open the destination entry modal
  $scope.showInputModal = function() {
    $scope.shownRoute = null;
    $scope.inputModal.show();
  };

  // Close the destination entry modal
  $scope.closeInputModal = function() {
    $scope.inputModal.hide();
    $scope.modalErrorMessage = "";
  };

}]);

//FILTERS
app.filter('time', function(){
  return function(input){
     var hours = input.substring(8, 10);
     var minutes = input.substring(10, 12)
     return hours + ":" + minutes;
  };
});
