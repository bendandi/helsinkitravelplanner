/*describe("true", function(){
  it("Should be true", function(){
    expect(true).toBeTruthy();
  });
});**/

describe("JourneyPlannerController", function(){
  beforeEach(module('app'));

  var $controller;

  beforeEach(inject(function(_$controller_){
      $controller = _$controller_;
  }));

  describe('$scope.title', function(){
    it('has title defined initially', function(){
      var $scope = {};
      var controller = $controller('JourneyPlannerCtrl', {$scope: $scope});
      expect($scope.title).toEqual("Helsinki Journey Planner");
    });
  });

  /*describe('APIRequests', function(){
    var $httpBackend = null;

    beforeEach(inject(function(_$httpBackend_) {
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    it('test HTTP', function(){
      $httpBackend.expectGET('data.json').respond();
    });

  });*/


});


describe("JourneyPlannerFilters", function(){
  beforeEach(module('app'));

  var $filter;

  beforeEach(inject(function(_$filter_){
    $filter = _$filter_;
  }));

  describe('time filter', function(){

    it('takes a YYYYMMDDHHMM string and returns time formatted hh:mm', function(){
      var time = $filter('time');
      expect(time('201402271235')).toEqual('12:35');
    })
  });

});
