A quick little AngularJS app written to try out the Ionic hybrid mobile application framework. 

It uses the Helsinki Reittopas API to look up public transport routes between two locations.

Current android apk available at
https://www.dropbox.com/s/iji3heop45actcf/HJP.apk?dl=0